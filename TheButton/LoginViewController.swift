//
//  Login.swift
//  TheButton
//
//  Created by Zac Holland on 3/10/17.
//  Copyright © 2017 Diericx. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnCallback(_ sender: Any) {
        FIRAuth.auth()?.signIn(withEmail: usernameField.text!, password: passwordField.text!) { (user, error) in
            print(error);
            if (error == nil) {
                self.performSegue(withIdentifier: "segueToGame", sender: sender)
            }
        }
    }
    
}
