//
//  ViewController.swift
//  TheButton
//
//  Created by Zac Holland on 3/10/17.
//  Copyright © 2017 Diericx. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        FIRApp.configure()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginBtnCallback(_ sender: Any) {
        performSegue(withIdentifier: "segueToLogin", sender: sender)
    }

}

