//
//  GameViewController.swift
//  TheButton
//
//  Created by Zac Holland on 3/10/17.
//  Copyright © 2017 Diericx. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import PubNub
import AVFoundation

class GameViewController: UIViewController, PNObjectEventListener, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    var client: PubNub!
    var username: String = "";
    var timer = Timer()
    var progressValue: Float = 0;
    var progressDelta: Float = 0.01;
    //video capture variables
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    //objects
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var cameraView: UIView!
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let configuration = PNConfiguration(publishKey: "pub-c-9598bf00-2785-41d4-ad2f-d2362b2738d9", subscribeKey: "sub-c-8a0a7138-e751-11e6-94bb-0619f8945a4f")
        
        //begin function for progress bar
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GameViewController.updateProgressBar), userInfo: nil, repeats: true);
        
        //configure firebase
        var ref: FIRDatabaseReference!
        ref = FIRDatabase.database().reference()
        
        //configure pubnub
        configuration.TLSEnabled = true
        self.client = PubNub.clientWithConfiguration(configuration)
        // Do any additional setup after loading the view, typically from a nib.
        
        /**
         Subscription process results arrive to listener which should adopt to PNObjectEventListener protocol
         and registered using:
         */
        self.client.addListener(self);
        self.client.subscribeToChannels(["global"], withPresence: false)
        
        //Get the user's username
        let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["username"] as? String ?? ""
            self.username = username;
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        captureSession = AVCaptureSession();
        captureSession?.sessionPreset = AVCaptureSessionPreset1920x1080;
        
        var backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error: NSError?
        var input = try? AVCaptureDeviceInput(device: backCamera);
        if (captureSession?.canAddInput(input))! {
            captureSession?.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        }
    }
    
    func updateProgressBar() {
        progressValue -= progressDelta;
        progressBar.value = progressValue;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func theButtonPressCallback(_ sender: Any) {
        //var json = JsonConvert.SerializeObject(bcm);
        if (username == "") {
            return
        }
        var json: String = "{\"action\": \"button-pressed\", \"username\": \"\(username)\"}";
        print(json);
        self.client.publish(json, toChannel: "global",
                            compressed: false, withCompletion: { (status) in
                                
                                if !status.isError {
                                    
                                    // Message successfully published to specified channel.
                                }
                                else{
                                    
                                    /**
                                     Handle message publish error. Check 'category' property to find
                                     out possible reason because of which request did fail.
                                     Review 'errorData' property (which has PNErrorData data type) of status
                                     object to get additional information about issue.
                                     
                                     Request can be resent using: status.retry()
                                     */
                                }
        })
    }

    
    
    // Handle new message from one of channels on which client has been subscribed.
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        
        // Handle new message stored in message.data.message
        if message.data.channel != message.data.subscription {
            
            // Message has been received on channel group stored in message.data.subscription.
            //print("-----FIRST TIPE OF DATA-----");
            print(message.data);
        }
        else {
            //print("-----FIRST TIPE OF DATA-----");
            // Message has been received on channel stored in message.data.channel.
            print(message.data);
        }
        
        let dictionary: AnyObject = message.data.message as AnyObject;
        let action: String = dictionary["action"] as! String;
        if (action == "button-pressed") {
            let name: String = dictionary["username"] as! String;

//            let square = UIView()
//            square.frame = CGRect(x: 55, y: 300, width: 20, height: 20)
//            square.backgroundColor = UIColor.red;
            // CGRectMake has been deprecated - and should be let, not var
            let randX = arc4random_uniform(UInt32(self.view.bounds.width));
            print(randX)
            let label = UILabel(frame: CGRect(x: Int(randX), y: 200, width: 200, height: 21))
            // you will probably want to set the font (remember to use Dynamic Type!)
            label.font = UIFont.preferredFont(forTextStyle: .footnote)
            // and set the text color too - remember good contrast
            label.textColor = .black
            // may not be necessary (e.g., if the width & height match the superview)
            // if you do need to center, CGPointMake has been deprecated, so use this
            label.center = CGPoint(x: Int(randX), y: 284)
            // this changed in Swift 3 (much better, no?)
            label.textAlignment = .center
            label.text = name
            self.view.addSubview(label)
            
            UIView.animate(withDuration: 3.0, animations: {
                label.center.y -= self.view.bounds.height*1.5
            })
//
//            let path = UIBezierPath()
//            path.move(to: CGPoint(x: 200,y: 239))
//            
//            let anim = CAKeyframeAnimation(keyPath: "position")
//            path.addCurve(to: CGPoint(x: 200, y: 239), controlPoint1: CGPoint(x: 200, y: 239), controlPoint2: CGPoint(x: 200, y: -500))
//            
//            anim.path = path.cgPath;
//            anim.rotationMode = kCAAnimationRotateAuto
//            anim.repeatCount = 0
//            anim.duration = 5.0
//            label.layer.add(anim, forKey: "animate position along path")
        }
    }
    
    // Handle subscription status change.
    func client(_ client: PubNub, didReceive status: PNStatus) {
        
        if status.operation == .subscribeOperation {
            
            // Check whether received information about successful subscription or restore.
            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
                
                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
                if subscribeStatus.category == .PNConnectedCategory {
                    
                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
                }
                else {
                    
                    /**
                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
                     an error but there is no longer any issue.
                     */
                }
            }
            else if status.category == .PNUnexpectedDisconnectCategory {
                
                /**
                 This is usually an issue with the internet connection, this is an error, handle
                 appropriately retry will be called automatically.
                 */
            }
                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
                // network.
            else {
                
                let errorStatus: PNErrorStatus = status as! PNErrorStatus
                if errorStatus.category == .PNAccessDeniedCategory {
                    
                    /**
                     This means that PAM does allow this client to subscribe to this channel and channel group
                     configuration. This is another explicit error.
                     */
                }
                else {
                    
                    /**
                     More errors can be directly specified by creating explicit cases for other error categories
                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                     or `PNNetworkIssuesCategory`
                     */
                }
            }
        }
        else if status.operation == .unsubscribeOperation {
            
            if status.category == .PNDisconnectedCategory {
                
                /**
                 This is the expected category for an unsubscribe. This means there was no error in
                 unsubscribing from everything.
                 */
            }
        }
        else if status.operation == .heartbeatOperation {
            
            /**
             Heartbeat operations can in fact have errors, so it is important to check first for an error.
             For more information on how to configure heartbeat notifications through the status
             PNObjectEventListener callback, consult http://www.pubnub.com/docs/ios-objective-c/api-reference#configuration_basic_usage
             */
            
            if !status.isError { /* Heartbeat operation was successful. */ }
            else { /* There was an error with the heartbeat operation, handle here. */ }
        }
    }
    
}

class ButtonPressMsg {
    var action: String = ""
    var name: String = ""
    
    init(action: String, name: String) {
        self.action = action
        self.name = name
    }
}
